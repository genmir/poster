class Post < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  def add_comment(par_com_id, user_com_id, text)
    comment = self.comments.create(parent_id: par_com_id, user_id: user_com_id, text: text)
    if par_com_id.nil?
      self.comments_schema << {
        'id' => comment.id,
        'text' => comment.text,
        'user' => comment.user.slice(:id, :email),
        'children' => []
      }
      self.save
      broadcast
      return comment
    end

    parent_comment = find_comment(self.comments_schema, par_com_id.to_i)
    if parent_comment.nil?
      # rare case when parent not found
      comment.update parent_id: nil
      self.comments_schema << {
        'id' => comment.id,
        'text' => comment.text,
        'user' => comment.user.slice(:id, :email),
        'children' => []
      }
    else
      parent_comment['children'] << {
        'id' => comment.id,
        'text' => comment.text,
        'user' => comment.user.slice(:id, :email),
        'children' => []
      }
    end
    self.save
    broadcast
    comment
  end

  def broadcast
    PostsChannel.broadcast_to self, self.post_data
  end

  def post_data
    data = self.slice(:title, :body, :created_at)
    data[:user] =self.user.slice(:id, :email)
    data[:comments] = self.comments_schema
    data
  end

  def kill_all_comments # temporary helper
    self.comments = []
    self.comments_schema = []
    self.save
  end

  private

  def find_comment(list, id)
    list.each do |el|
      return el if el['id'] == id
      found = find_comment(el['children'], id)
      return found if found.present?
    end
    return nil
  end
end
