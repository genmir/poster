class Api::ApiController < ActionController::Base
  skip_before_action :verify_authenticity_token

  before_action :require_token


  def require_token
    return if params[:token] == Rails.application.credentials.api_token
    render json: {
      success: false,
      errors: ['Access denied']
    }
  end

end
