class Api::PostsController < Api::ApiController
  before_action :set_post, only: [:show, :add_comment]

  def show
    render json: {
      success: true,
      post: @post.post_data
    }
  end

  def add_comment
    @post.add_comment(params[:parent_id], params[:user_id], params[:text])
    render json: {
      success: true,
      post: @post.post_data
    }
  end

  private

  def add_copmment_params
    params.permit(:parent_comment, :text)
  end

  def set_post
    @post = Post.find_by id: params[:id]
    if @post.nil?
      render json: {
        success: false,
        errors: ['Post not found']
      }
    end
  end

end
