export default function(post = null, action) {
  switch (action.type) {
    case 'FETCH_POST':
      return action.payload.data.post;
      break;
    case 'UPDATE_POST':
      return action.payload;
      break;
    default:
      return post;
  }
}
