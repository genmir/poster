import React from "react";
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';

import thunk from 'redux-thunk';

import Post from './Post';
import reducers from '../reducers';

const store = createStore(reducers, applyMiddleware(thunk));

class PostPage extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <Post post_id={this.props.post_id} />
      </Provider>
    );
  }
}

export default PostPage;
