import React from "react";
import { createStore } from 'redux';
import { connect } from 'react-redux';

// import { ActionCableConsumer } from 'react-actioncable-provider';
import actioncable from 'actioncable';

import CommentsList from './CommentsList';
import { fetchPost, updatePost } from '../actions';
// import { API_ROOT } from '../constants';

class Post extends React.Component {

  componentDidMount() {
    this.props.fetchPost(this.props.post_id);

    this.cable = actioncable.createConsumer('ws://localhost:3000/cable');
    this.postChannel = this.cable.subscriptions.create({
                channel: 'PostsChannel',
                post_id: this.props.post_id
            },{
                connected: () => { console.log('connected'); },
                disconnected: () => { console.log('disconnected'); },
                received: data => {
                  console.log(data);
                  this.props.updatePost(data);
                }
            });
  }

  handleReceivedPost = response => {
    console.log('handleReceivedPost');
    console.log(response);
    const { post } = response;
    this.setState({
      post: post
    });
  };

  handleSubscribed = response => {
    console.log('handleSubscribed');
    console.log(response);
  };

  render () {
    if(this.props.post === undefined || this.props.post === null) {
        return (
          <h3 className="my-5 text-center">
            Loading...
          </h3>
        );
    } else {
      return (
        <div className="card my-3">
          <div className="card-header">
            <div className="text-info">{this.props.post.user.email}:</div>
            {this.props.post.title}
          </div>
          <div className="card-body">
            <p>{this.props.post.body}</p>
          </div>

          <div className="card-footer">
            <p>Comments:</p>
            <CommentsList comments={this.props.post.comments} />
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return { post: state.post };
};

export default connect(
  mapStateToProps,
  { fetchPost, updatePost }
)(Post);
