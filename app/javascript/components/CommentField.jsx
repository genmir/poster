import React from "react";
import { createStore } from 'redux';
import { connect } from 'react-redux';

class CommentField extends React.Component {

  render () {
    return (
      <div className="mt-2 ml-5" id={"answer_to_"+this.props.parentId}>
        <textarea className="form-control mb-1" rows="2"></textarea>
        <div className="btn btn-sm btn-info float-right" onClick={this.addComment}>Answer</div>
        <div className="clearfix"></div>
      </div>
    );
  }
}

export default CommentField;
