import React from "react";
import { createStore } from 'redux';
import { connect } from 'react-redux';

import Comment from './Comment';

class CommentsList extends React.Component {

  render () {
    return this.props.comments.map( comment => {
          return (
            <Comment key={"comment_" + comment.id} theComment={comment} />
          );
        });
  }
}

export default CommentsList;
