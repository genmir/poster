import React from "react";
import { createStore } from 'redux';
import { connect } from 'react-redux';

import CommentsList from './CommentsList';
import CommentField from './CommentField';

class Comment extends React.Component {

  render () {
    return (
      <div className="mt-2">
        <div className="text-info">{this.props.theComment.user.email}:</div>
        <div>{this.props.theComment.text}</div>
        <CommentField parentId={this.props.theComment.id} />
        <div className="ml-5 mt-2">
          <CommentsList key={'comment_' + this.props.theComment.id} comments={this.props.theComment.children} />
        </div>
      </div>
    );
  }
}

export default Comment;
