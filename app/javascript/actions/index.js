import postApi from '../apis/post';

export const fetchPost = (postId) => {
  return async (dispatch, getState) => {
    const response = await postApi.post('?id='+postId);
    dispatch({ type: 'FETCH_POST', payload: response });
  }
}

export const updatePost = (data) => {
  return (dispatch, getState) => {
    dispatch({ type: 'UPDATE_POST', payload: data });
  }
}
