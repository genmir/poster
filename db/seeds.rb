# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.find_by(email: 'user1@example.com') || User.create(email: 'user1@example.com', password: 'password')
user2 = User.find_by(email: 'user2@example.com') || User.create(email: 'user2@example.com', password: 'password')
user3 = User.find_by(email: 'user3@example.com') || User.create(email: 'user3@example.com', password: 'password')


post1 = Post.find_by(title: 'First post') || Post.create(user: user1, title: 'First post', body: 'First post body')

comm1 = post1.comments.find_by(text: 'Comment 1') || post1.add_comment(nil, user2.id, 'Comment 1')
comm2 = post1.comments.find_by(text: 'Comment 2') || post1.add_comment(nil, user3.id, 'Comment 2')
comm3 = post1.comments.find_by(text: 'Comment 3') || post1.add_comment(comm1.id, user1.id, 'Comment 3')
