class AddCommentsSchemaToPost < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :comments_schema, :jsonb, null: false, default: []
  end
end
