class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.references :post, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.bigint :parent_id, index: true
      t.text :text, null: false, default: ''

      t.timestamps
    end
  end
end
