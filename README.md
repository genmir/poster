# README

### Comments schema

```
[
  { id: 34,
    children: [
      { id: 45, children: [] },
      {
        id: 46,
        children: [
          { id: 56, children: [] },
          { id: 57, children: [] },
        ]
      }
    ]
  },
  { id: 35, children: [] },
]
```


## API /posts

### GET http://localhost:3000/api/posts/7?token=the_api_token

Example response:
```json
{
    "success": true,
    "post": {
        "id": 7,
        "title": "First post",
        "body": "First post body",
        "user": {
            "id": 1,
            "email": "user1@example.com"
        },
        "comments": [
            {
                "id": 18,
                "children": [
                    {
                        "id": 20,
                        "children": [],
                        "text": "Comment 3",
                        "user": {
                            "id": 1,
                            "email": "user1@example.com"
                        }
                    }
                ],
                "text": "Comment 1",
                "user": {
                    "id": 2,
                    "email": "user2@example.com"
                }
            },
            {
                "id": 19,
                "children": [],
                "text": "Comment 2",
                "user": {
                    "id": 3,
                    "email": "user3@example.com"
                }
            }
        ]
    }
}
```

## Adding react to old app

Add to Gemfile
```
gem 'react-rails'
```

```
bundle install
rails webpacker:install
rails webpacker:install:react
rails generate react:install
```

Create directories for React stuff:
```
mkdir app/javascript/actions
mkdir app/javascript/apis
mkdir app/javascript/components
mkdir app/javascript/reducers
```

Add Redux:
```
yarn add redux react-redux axios redux-thunk
```

Add dumb component:
```javascript
// app/javascript/components/Post.jsx

import React from "react";
import { createStore } from 'redux';
import { connect } from 'react-redux';

class Post extends React.Component {

  render () {
    return (
      <div className="card my-3">
        <div className="card-body">
          Post with ID {this.props.post_id}
        </div>
      </div>
    );
  }
}

export default Post;

```

and insert component in page's code:
```ruby
= react_component('Post', { post_id: @post.id })
```
