Rails.application.routes.draw do
  root 'posts#index'
  resources :posts
  namespace :api do
    post 'posts/show'
    post 'posts/add_comment'
  end

  mount ActionCable.server => '/cable'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
